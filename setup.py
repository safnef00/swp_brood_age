from setuptools import setup, find_packages

setup(
    name='swp-data-processing',
    version='1.0.0',
    description='Process classified brood cell data provided by Lino Steinhau\' s thesis to prepare regressor '
                'training data.',
    author='Frederik S, Lukas H',
    author_email='safnef00@zedat.fu-berlin.de',
    packages=find_packages(),
    install_requires=[
        "matplotlib", "numpy"
    ],
    package_dir={'': '.'},
)
