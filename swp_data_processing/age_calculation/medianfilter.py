import numpy as np

from age_calculation.cell_classification_enum import CellClassification

WINDOW_SIZE_IN_SECONDS = 60*60*24
MIN_ELEMENTS_IN_WINDOW = 10


def median_filter(cell_classifications: {}) -> {}:
    """Filters noise from cell_classifications for a single cell using the most-frequent-element filter.

       Args:
           cell_classifications (dict): The cell_classifications for a single cell.

       Returns:
           dict: The noise-filtered cell_classifications.
       """

    filtered_cell_classifications = {}
    # Iterate over each timestamp, get window and calculate most frequent label to reduce noise.
    for timestamp in cell_classifications:
        labels_in_window = get_labels_in_window(timestamp, WINDOW_SIZE_IN_SECONDS, cell_classifications)
        # If too few labels are in the window, the probability for noise rises, so we label them as noise.
        if len(labels_in_window) < MIN_ELEMENTS_IN_WINDOW:
            filtered_cell_classifications[timestamp] = CellClassification.NOISE
            continue
        most_frequent_label = get_most_frequent_label_in_window(labels_in_window)
        # ESSENTIAL STEP: Save new label.
        filtered_cell_classifications[timestamp] = most_frequent_label
    return filtered_cell_classifications


def get_labels_in_window(timestamp: int, window_size: int, cell_classifications: {}) -> []:
    """Gets a timestamp and the cell classifications with respect to a cell.

        Args:
            timestamp (int): The timestamp of the center of the window.
            window_size (int): The window size in seconds
            cell_classifications: The cell classifications.

        Returns:
            list: All labels in the window defined by the window size and timestamp.
        """
    np_array = np.array(list(cell_classifications.items()))  # SPEEDUP

    timestamp_min = timestamp - window_size
    timestamp_max = timestamp + window_size
    labels_in_window = []

    for (ts, class_label) in np_array:
        if timestamp_min <= ts <= timestamp_max:  # SPEEDUP
            labels_in_window.append(class_label)
    return labels_in_window


def get_most_frequent_label_in_window(array) -> CellClassification:
    np_array = np.asarray(array)  # SPEEDUP
    # find frequency of each value
    values, counts = np.unique(np_array, return_counts=True)

    # get value with the highest frequency
    return values[counts.argmax()]
