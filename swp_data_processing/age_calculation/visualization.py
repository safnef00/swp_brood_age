from datetime import datetime

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib import ticker

from age_calculation.cell_classification_enum import CellClassification


def plot_classifications_for_cell(cell_predictions: dict, index: str, highlight_timestamp=None,
                                  cell_age_estimations: dict = None, show=True):
    # Extract x and y data from the list
    x_data, y_data = [], []
    for timestamp in cell_predictions:
        class_label: CellClassification = cell_predictions[timestamp]
        x_data.append(datetime.utcfromtimestamp(timestamp))
        y_data.append(class_label.value)

    # Convert x_data from datetime to numerical values
    x_data = mdates.date2num(x_data)

    # Create a figure and axis
    fig, ax = plt.subplots(figsize=(12, 6))
    ax2 = ax.twinx()  # Create a second y-axis

    # Set the x-axis formatter to display readable dates
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))

    # Set the x-axis locator to auto format the tick intervals
    ax.xaxis.set_major_locator(mdates.AutoDateLocator())

    # Plot the data with different colors based on the insect stage
    for stage in CellClassification:
        x_stage = [x for x, y in zip(x_data, y_data) if y == stage.value]
        y_stage = [y for y in y_data if y == stage.value]
        ax.plot_date(x_stage, y_stage, label=stage.name, fmt='|')

    # Highlight the specified timestamp if provided
    if highlight_timestamp is not None:
        x_highlight = datetime.utcfromtimestamp(highlight_timestamp)
        y_highlight = CellClassification.HAS_OLD_PUPA.value
        ax.plot_date(x_highlight, y_highlight, label='critical point', fmt='x', color='red', linewidth=3.0)

    # Plot the cell age estimations on the second y-axis if provided
    if cell_age_estimations is not None:
        x_age = [datetime.utcfromtimestamp(timestamp) for timestamp in cell_age_estimations]
        y_age = [cell_age_estimations[timestamp] for timestamp in cell_age_estimations]
        ax2.plot_date(x_age, y_age, label='Cell Age', fmt='|', color='black')

        # Show a grid in the background
        ax2.grid(True)
        # Set integer ticks on ax2 (second y-axis)
        ax2.yaxis.set_major_locator(ticker.MaxNLocator(integer=True))

    # Set custom labels
    ax.set_yticks([stage.value for stage in CellClassification])
    ax.set_yticklabels([stage.name for stage in CellClassification])

    ax.set_title("Cell Classification & predicted Age for " + index)

    # Rotate the x-axis labels for better visibility
    fig.autofmt_xdate()

    # Show the plot
    plt.savefig("plots/cell-" + index + ".png")
    if show:
        plt.show()
    plt.close()
