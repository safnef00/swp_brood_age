from age_calculation.cell_classification_enum import CellClassification as CellEnum
from age_calculation.medianfilter import get_labels_in_window

WINDOW_SIZE_IN_SECONDS = 60 * 60 * 12  # 12 hours
AGE_BEGINNING_OF_OLD_PUPA_IN_DAYS = 21 - 3
AGE_END_OF_OLD_PUPA_IN_DAYS = 21


def guess_age(filtered_cell: {}) -> (int, int):
    """
    Estimates the age of a cell.

    :param filtered_cell: noise filtered cell data
    :return: (at certain timestamp; estimated age of the cell in days), (None, None if it can't estimate)
    """
    earliest_old_pupa = None
    latest_old_pupa = None
    # Find earliest and latest HAS_OLD_PUPA
    for timestamp in filtered_cell:
        if filtered_cell[timestamp] != CellEnum.HAS_OLD_PUPA:
            continue
        if earliest_old_pupa is None or timestamp < earliest_old_pupa:
            earliest_old_pupa = timestamp
        if latest_old_pupa is None or timestamp > latest_old_pupa:
            latest_old_pupa = timestamp
    # No HAS_OLD_PUPA found -> can't estimate age -> return None.
    if earliest_old_pupa is None or latest_old_pupa is None:
        return None, None
     # If EMPTY or HAS_BEE_HEAD is near the latest point -> estimate age from there
    nearby_labels = get_labels_in_window(latest_old_pupa, WINDOW_SIZE_IN_SECONDS, filtered_cell)
    if CellEnum.EMPTY in nearby_labels or CellEnum.HAS_BEE_HEAD in nearby_labels:
        return latest_old_pupa, AGE_END_OF_OLD_PUPA_IN_DAYS
    # If HAS_YOUNG_PUPA is near the earliest point -> estimate age from there
    nearby_labels = get_labels_in_window(earliest_old_pupa, WINDOW_SIZE_IN_SECONDS, filtered_cell)
    if CellEnum.HAS_YOUNG_PUPA in nearby_labels:
        return earliest_old_pupa, AGE_BEGINNING_OF_OLD_PUPA_IN_DAYS


def infer_age(cell_classifications: {}, critical_timestamp, age_in_days) -> {int: int}:
    """
    Calculate the age at each timestamp by using the estimated age at one timestamp.

    :param cell_classifications: Dictionary of timestamps and classifications, noise filtered or original.
    :param critical_timestamp: The timestamp where we know the estimated age of.
    :param age_in_days: The age in days for the timestamp.
    :return: Dictionary with all original timestamps and the infered age in days (rounded).
    """
    cell_age_estimations = {}
    for timestamp in cell_classifications:
        distance = critical_timestamp - timestamp
        distance_days = round(distance / (60.0 * 60 * 24))
        age_at_timestamp = age_in_days - distance_days
        if 0 <= age_at_timestamp <= 21:
            cell_age_estimations[timestamp] = age_at_timestamp
    return cell_age_estimations
