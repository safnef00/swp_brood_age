# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import json

import numpy as np

import visualization, medianfilter as mf, cell_classification_enum
from age_calculation import guess_age

IMAGE_FORMAT = [18, 16]


def load_json_file(file_path):
    with open(file_path, 'r') as file:
        data = json.load(file)
    return data


def cell_count_for_each_file(json_file: {}) -> {str, int}:
    counting_dict = {}
    for file in json_file:
        file_name = file["filename"]
        counting_dict[file_name] = len(file["cells"])  # * len(file["cells"][0])
    return counting_dict


def process_data(json_data: {}) -> np.array:
    # Create empty 2d-array in the same shape as the cells on the images.
    cell_classifications = np.empty(IMAGE_FORMAT, dtype=dict)
    # Iterate over each file (each file has specific date).
    for file in json_data:
        # When the scan has been taken.
        timestamp = int(file["time"])
        # 2d-array used for storing the cell classifications. First array = rows.
        cells: [] = file["cells"]
        for row_index in range(len(cells)):
            row = cells[row_index]
            for column_index in range(len(row)):
                # Here we know have the (row, column) of a cell wrt. a scan taken at a specific time stamp.
                # Init dictionary for a cell, if not already existing, to save classification label per timestamp.
                if cell_classifications[row_index, column_index] is None:
                    cell_classifications[row_index, column_index] = {}
                # Predictions for a cell on specific scan image in original json data:
                cell_predictions: {} = cells[row_index][column_index]
                # ESSENTIAL STEP: Save prediction at this timestamp to prediction dictionary for this cell.

                class_string = cell_predictions["pred_label"]
                class_enum = cell_classification_enum.class_label_to_enum(class_string)
                if class_enum is None:
                    # Under certain conditions the label cannot be converted to an enum, so we skip this entry.
                    # Possible conditions are multiple labels or no label at all.
                    continue
                cell_classifications[row_index, column_index][timestamp] = class_enum
    return cell_classifications


def main():
    data = load_json_file("full_dataset_predictions.json")
    cell_array = process_data(data)
    row_count = IMAGE_FORMAT[0]
    column_count = IMAGE_FORMAT[1]

    # Create empty 2d-array in the same shape as the cells to save the age predictions.
    cell_age_array = np.empty(IMAGE_FORMAT, dtype=dict)

    for row in range(row_count):  # TODO: row_count
        for column in range(column_count):  # TODO: column_count
            cell_index_string = "cell [" + str(row) + ", " + str(column) + "]"
            print(cell_index_string)
            # Visualize current data
            visualization.plot_classifications_for_cell(cell_array[row, column], cell_index_string + "-01", show=False)
            # Apply noise filtering
            filtered_cell = mf.median_filter(cell_array[row, column])
            visualization.plot_classifications_for_cell(filtered_cell, cell_index_string + "-02", show=False)
            # Try to calculate age at key point
            critical_timestamp, age_in_days = guess_age.guess_age(filtered_cell)
            age_predictable = age_in_days is not None
            if age_predictable:
                print(cell_index_string + " age at " + str(critical_timestamp) + " = " + str(age_in_days))
                cell_ages = guess_age.infer_age(cell_array[row, column], critical_timestamp, age_in_days)
                cell_age_array[row, column] = cell_ages
                visualization.plot_classifications_for_cell(filtered_cell, cell_index_string + "-03",
                                                            highlight_timestamp=critical_timestamp, show=False,
                                                            cell_age_estimations=cell_ages)

    # Save cell_age_array to JSON file
    list_of_dicts = cell_age_array.tolist()
    with open('cell_age_predictions.json', 'w') as file:
        json.dump(list_of_dicts, file, indent=4)


if __name__ == '__main__':
    main()
