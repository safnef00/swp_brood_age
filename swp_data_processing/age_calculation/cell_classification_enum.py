from enum import Enum, auto

DEBUG_MESSAGES = False


class CellClassification(Enum):
    NOISE = auto()
    HAS_EGG = auto()
    HAS_LARVA = auto()
    HAS_YOUNG_PUPA = auto()
    HAS_OLD_PUPA = auto()
    HAS_BEE_HEAD = auto()
    EMPTY = auto()
    UNKNOWN = auto()

    def __lt__(self, other):
        stages = [
            CellClassification.HAS_EGG,
            CellClassification.HAS_LARVA,
            CellClassification.HAS_YOUNG_PUPA,
            CellClassification.HAS_OLD_PUPA,
            CellClassification.HAS_BEE_HEAD,
            CellClassification.EMPTY,
            CellClassification.UNKNOWN,
            CellClassification.NOISE
        ]
        return stages.index(self) < stages.index(other)


def class_label_to_enum(class_label: str) -> CellClassification:
    class_label = class_label.replace(" ", "_").removeprefix("(").removesuffix(")").upper()
    if class_label in CellClassification.__members__:
        return CellClassification[class_label]
    else:
        if DEBUG_MESSAGES:
            print("Could not convert label to enum:", class_label)
        return None
