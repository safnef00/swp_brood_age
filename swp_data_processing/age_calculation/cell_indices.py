"""
Cell Indices Module from Lino Steinhau.

Found in training.py, adjusted for our needs.
"""

import torchvision.transforms as transforms
import torchvision.transforms.functional as F


class ModifiedLabelboxDataset():
    resizedCellWidth = 64
    resizedCellHeight = 64
    numCellsY = 18
    numCellsX = 16
    offsetY = 248
    offsetX = 288
    centerOffsetX = 300
    centerOffsetY = 510
    evenRowOffsetX = 150
    imageWidth = 4992
    cellSize = 400
    cellWidth = cellSize
    cellHeight = cellSize

    def getCellsFromImage(self, image):
        indices = self.getCellIndices()
        cells = []
        for ((startX, endX), (startY, endY)), (x, y) in indices:
            tensor = image[startY: endY, startX: endX].permute(2, 0, 1)
            assert endX - startX > 0
            assert endY - startY > 0
            cells.append((transforms.functional.resize(tensor, [self.resizedCellWidth, self.resizedCellHeight]), ((startX, endX), (startY, endY)),
                          (x, y)))
        return cells

    def getCellIndices(self):
        indices = []
        for y in range(self.numCellsY):
            for x in range(self.numCellsX):
                startY = y * self.offsetY + self.centerOffsetY - self.cellHeight // 2
                startX = (
                        (0 if y % 2 == 0 else self.evenRowOffsetX)
                        + x * self.offsetX
                        + self.centerOffsetX
                        - self.cellWidth // 2
                )
                startX -= 40
                startY -= 70
                indices.append((((startX, startX + self.cellWidth), (startY, startY + self.cellHeight)), (x, y)))
        return indices

