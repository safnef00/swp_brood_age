"""
OpenCV Template Module.

For classes that need to use certain image functionalities, like loading an image, showing the current version,
cropping the image, saving to a path. Template class by Frederik; Image cropping method by Malte.
"""

import cv2
import numpy as np


class OpenCVTemplate:
    """
    This class provides basic functionality like loading a picture with OpenCV, showing it in between or saving it.
    After calling a method the current picture may get opened, depending on the value of show_image_on_change.
    Therefore on_change() has to be called at the end of each implemented method that changes the image. The on_change
    method may be overwritten.
    """
    image = None
    show_image_on_change = None
    image_show_size = None

    def __init__(self, image_path, show_image_on_change=False, image_show_size=(1200, 1200)):
        """
        Loads image at the given path.

        Args:
            image_path: Path of the image.
            show_image_on_change: If the image should be shown after changes to the image. (default=False)
            image_show_size: The size of the shown image. (default=(1200, 858))

        Raises:
            May throw Exception if image could not be loaded.
        """
        self.image = cv2.imread(image_path)
        self.show_image_on_change = show_image_on_change
        self.image_show_size = image_show_size
        if self.image is None:
            raise Exception("Something went wrong while loading the image!")

    def crop_image(self, x_start=None, x_end=None, y_start=None, y_end=None, width=None, height=None):
        """
        This function crops the image instance EITHER on the given coordinates OR
        starting on point (x_start,y_start) and going width-pixels right and height_pixels down.
        The original image stays the same.

        Args:
            x_start (int):      x coordinate for top left corner of the ROI
            y_start (int):      y coordinate for top left corner of the ROI
            x_end (int):        x coordinate for bottom right corner of the ROI
            y_end (int):        y coordinate for bottom right corner of the ROI
            width (int):        number of pixels going down from the top left corner (x_start,y_start)
            height (int):       number of pixels going right from the top left corner (x_start,y_start)

        Returns:
            roi (numpy.ndarray): The cut out area / region of interest

        Raises:
            TypeError: if the coordinates or width or height are not of type int
            TypeError: if the image is not an numpy.ndarray
            ValueError: if the coordinates or width or height are not valid
        """

        if not isinstance(self.image, type(np.array([0]))):
            raise TypeError("The image must be of type numpy.ndarray. But is", type(self.image))
        if ((type(x_start) not in [type(None), int])
                or (type(y_start) not in [type(None), int])
                or (type(x_end) not in [type(None), int])
                or (type(y_end) not in [type(None), int])
                or (type(width) not in [type(None), int])
                or (type(height) not in [type(None), int])):
            raise TypeError("The coordinates and width and height must be of type int.",
                            type(x_start), type(y_start), type(x_end), type(y_end), type(width), type(height))

        if x_start is None or y_start is None:
            raise ValueError("""You need to provide x- and
                            y-coordinates (upper left corner) to start from!""")
        if (x_end is None or y_end is None) and (width is None or height is None):
            raise ValueError("""You need to either provide x- and y-coordinates
                            for the bottom right corner or width and height.""")
        if x_start < 0 or y_start < 0:
            raise ValueError("x_start and y_start must be greate or equal zero (>=0)")

        shape = self.image.shape

        if width is not None:
            x_end = x_start + height - 1
            y_end = y_start + width - 1

        if x_start > x_end or y_start > y_end:
            raise ValueError(
                "x_end/y_end must be greater than x_start/y_start and width/height must be greate than zero.")

        if y_end >= shape[0]:
            raise ValueError("The y_end coordinate or the height you provided is out of the image bounds. Image width:",
                             shape[0], "; y_end:", y_end)
        if x_end >= shape[1]:
            raise ValueError("The x_end coordinate or the width you provided is out of the image bounds. Image width:",
                             shape[1], "; x_end:", x_end)

        # crop/extract face from original image
        shape = self.image.shape
        if y_end == shape[0] - 1:
            if x_end == shape[1] - 1:
                roi = self.image[y_start:, x_start:]
            else:
                roi = self.image[y_start:, x_start:x_end]
        else:
            if x_end == shape[1] - 1:
                roi = self.image[y_start:y_end, x_start:]
            else:
                roi = self.image[y_start:y_end, x_start:x_end]
        return roi

    def on_change(self):
        """
        Has to be called at the end of every method that changes the image. May be overwritten.
        """
        if self.show_image_on_change:
            self.show_image()

    def show_image(self, window_name="Image"):
        """
        Shows image until a key gets pressed. The edited image stays in the same size.
        """
        resized = cv2.resize(self.image, self.image_show_size)
        cv2.imshow(window_name, resized)
        cv2.waitKey(0)

    def save_image(self, path):
        """
        Saves the edited image to a given path. But be aware that no new directories get created.

        Args:
            path: The path including the name where the image should be saved at.
        """
        assert self.image is not None
        cv2.imwrite(path, self.image)
