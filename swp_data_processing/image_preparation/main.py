import glob
import json

import cv2
import numpy as np

from age_calculation.cell_indices import ModifiedLabelboxDataset
from image_preparation import utils
from image_template import OpenCVTemplate

BROOD_SCANS_DIRECTORY_PATH = "/Users/fsafner/Documents/Uni/Master/Semester/2-SS/Module SS/SWP/bn_classifier_data-main/data"
AGE_PREDICTIONS_PATH = "./age_calculation/cell_age_predictions.json"


def main():
    # Load age prediction json
    with open(AGE_PREDICTIONS_PATH) as json_file:
        cell_age_array = json.load(json_file, object_pairs_hook=lambda pairs: {int(key): value for key, value in pairs})

    # Process cell indizes
    indices = ModifiedLabelboxDataset().getCellIndices()

    # Prepare X, Y labels for training
    X = []
    Y = []

    # Iterate over each brood scan image from the image directory
    image_paths = glob.glob(BROOD_SCANS_DIRECTORY_PATH + "/*.png")
    print("Loaded image paths: " + str(len(image_paths)))
    for image_path in image_paths:
        print("Observing image: " + image_path)
        opencv_image = OpenCVTemplate(image_path)

        # Load unix time of this image
        unix_time = utils.image_path_to_unix(image_path)
        if unix_time is None:
            print("Could not convert image_path to unix time! Exiting program.")
            return

        added_samples_counter = 0
        # Iterate over each cell of the image, cut out, shrink and save cutout + label.
        for index in range(len(indices)):
            ((startX, endX), (startY, endY)), (y, x) = indices[index]  # (y,x) as getCellIndices() used row x column
            cell_index_string = "CELL[" + str(x) + ", " + str(y) + "]"
            #print(cell_index_string + ": startX = ", startX, "endX =", endX, "startY =", startY, "endY =", endY)

            # Check if age was predicted at all for this cell and
            # assure that exactly this timestamp has an age prediction (it could be outside the 0-21 days interval)
            # by accessing the loaded cell_age_predictions.json.
            cell_ages_dict = cell_age_array[x][y]
            if cell_ages_dict is None or unix_time not in cell_ages_dict:
                continue
            # Get age of the cell of this image.
            cell_age = cell_ages_dict[unix_time]

            # Crop / cut out the cell of the whole image and save original image to later undo the cropping.
            uncropped_image = opencv_image.image
            # Actually crop
            opencv_image.image = opencv_image.crop_image(x_start=startX, x_end=endX, y_start=startY, y_end=endY)
            #print(opencv_image.image.shape)
            # Downscale the cropped cell image
            resized_image = cv2.resize(opencv_image.image, dsize=(32, 32), interpolation=cv2.INTER_LINEAR)
            # ESSENTIAL STEP: SAVE CELL IMAGE AND CORRESPONDING AGE LABEL!
            X.append(resized_image)
            Y.append(cell_age)
            added_samples_counter += 1
            #print(opencv_image.image.shape)
            #print(opencv_image.image.dtype)
            # Undo cropping to provide full image on the next loop iteration.
            opencv_image.image = uncropped_image
            #opencv_image.show_image(cell_index_string + ": Age in days = " + str(cell_age))
        print("Added " + str(added_samples_counter) + " samples for current image.")

    # Convert X, Y to numpy arrays
    X = np.array(X)
    Y = np.array(Y)

    print("X.shape", X.shape)
    print("Y.shape", Y.shape)
    print("X.dtype", X.dtype)

    # Save X, Y to files
    np.save("X_data", X)
    np.save("Y_data", Y)


if __name__ == '__main__':
    main()
