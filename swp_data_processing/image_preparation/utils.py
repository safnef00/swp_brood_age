import re
from datetime import datetime


def image_path_to_unix(image_path):
    # Extract the date and time from the image path
    pattern = r'\d{6}-\d{6}-utc'
    match = re.search(pattern, image_path)
    if not match:
        return None

    date_str = match.group()
    date_format = '%y%m%d-%H%M%S-utc'

    try:
        # Convert the date string to a datetime object
        utc_datetime = datetime.strptime(date_str, date_format)
        # Convert the datetime object to Unix time
        unix_time = int(utc_datetime.timestamp())
        return unix_time
    except ValueError:
        return None


if __name__ == '__main__':
    # Example usage
    image_path = './image_preparation/scan_back_220817-110000-utc.png'
    unix_time = image_path_to_unix(image_path)
    if unix_time is not None:
        print(f"Unix time: {unix_time}")
    else:
        print("Invalid image path or date format")