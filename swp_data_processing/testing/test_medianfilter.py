import unittest

from age_calculation.cell_classification_enum import CellClassification as CellEnum


class MyTestCase(unittest.TestCase):
    def test_get_most_frequent_label_in_window(self):
        array = [0, 0, 0, 1, 1, 1, 1, 2]
        most_frequent = Medianfilter.get_most_frequent_label_in_window(array)
        self.assertEqual(most_frequent, 1)  # add assertion here

    def test_get_most_frequent_label_in_window_equal(self):
        array = [0, 0]
        most_frequent = Medianfilter.get_most_frequent_label_in_window(array)
        self.assertEqual(most_frequent, 0)  # add assertion here

    def test_get_most_frequent_label_in_window_enums(self):
        array = [CellEnum.HAS_LARVA, CellEnum.EMPTY, CellEnum.EMPTY, CellEnum.EMPTY, CellEnum.UNKNOWN, CellEnum.HAS_EGG]
        most_frequent = Medianfilter.get_most_frequent_label_in_window(array)
        self.assertEqual(most_frequent, CellEnum.EMPTY)  # add assertion here


if __name__ == '__main__':
    unittest.main()
